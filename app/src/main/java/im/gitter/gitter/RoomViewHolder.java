package im.gitter.gitter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import im.gitter.gitter.activities.MainActivity;
import im.gitter.gitter.utils.CircleNetworkImageView;

public class RoomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    // Having publicly accessible properties is not the java way, but we need speed for fast view changes
    public final CircleNetworkImageView avatarView;
    public final TextView titleView;
    public final BadgeView badgeView;
    public String roomId;

    RoomViewHolder(View avatarWithBadgeView) {
        super(avatarWithBadgeView);
        avatarView = (CircleNetworkImageView) itemView.findViewById(R.id.avatar);
        titleView = (TextView) itemView.findViewById(R.id.title);
        badgeView = (BadgeView) itemView.findViewById(R.id.badge);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (roomId != null) {
            Context context = itemView.getContext();
            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra(MainActivity.GO_TO_ROOM_ID_INTENT_KEY, roomId);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(intent);
        }
    }
}
